package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Salas {
	
	private static Connection conexion;
	private static String tabla = "Salas";
	private static String bd;
	
	private int codigo = 0;
	private String nombre = "";
	private int pelicula = 0;

	
	
	//CONSTRUCTORES
	public Salas(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
	}
	
	public Salas(Connection conexion, String bd, String nombre, int pelicula) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		this.pelicula = pelicula;
		crearSala(nombre, pelicula);	
	}
	

	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE IF NOT EXISTS "+ tabla +
					" (codigo INT PRIMARY KEY AUTO_INCREMENT, nombre NVARCHAR(100), "
					+ "pelicula INT,"
					+ " FOREIGN KEY(pelicula) REFERENCES Peliculas(codigo));";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
	}
	

	public static void crearSala(String nombre, int pelicula) {
		System.out.println("\n\nSala a crear con:\n - Nombre: " + nombre + "\n - Pelicula: "+pelicula);
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (nombre, pelicula) VALUE ("
						+ "\" "+ nombre +"\", "+pelicula+");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				System.out.println("	--> Datos introducidos correctamente\n");
			} catch (SQLException e) {
			
				System.out.println("No s'ha pogut introduir");
			}
	}

	
	
	//OBTENER CAJA
	public static void obtenerSalas() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getInt("codigo") +"\nNombre:" + resultSet.getString("nombre")+"\nPelicula:" + resultSet.getInt("pelicula"));
			}			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
		}		
	}
	
	


	
}


