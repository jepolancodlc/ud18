package dto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Peliculas {
	
	private static Connection conexion;
	private static String tabla = "Peliculas";
	private static String bd;
	
	private int codigo = 0;
	private String nombre = "";
	private int edad = 0;
	
	
	//CONSTRUCTORES
	public Peliculas(Connection conexion, String bd) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
	}
	
	public Peliculas(Connection conexion, String bd, String nombre, int edad) {
		this.conexion = conexion;
		this.bd = bd;
		createTable();
		this.nombre = nombre;
		this.edad = edad;
		crearPelicula(nombre, edad);	
		
	}
	
	
	//CREACI� TAULA
	public static void createTable() {
	
		try {
			String Querydb = "USE " + bd + ";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "CREATE TABLE IF NOT EXISTS "+ tabla +
					" (codigo INT PRIMARY KEY AUTO_INCREMENT, nombre NVARCHAR(100), "
					+ "edad INT);";
			
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Error creando tabla");
		}
		
	}
	
	
	//INSERCI� DE EMPLEATS
	public static void crearPelicula(String nombre, int edad) {
		System.out.println("Pelicula a crear con:\n - Nombre: " + nombre + "\n - Edad: "+edad);
			try {
				String Querydb = "USE "+bd+";";
				Statement stdb = conexion.createStatement();
				stdb.executeUpdate(Querydb);
				
				String Query = "INSERT INTO " + tabla + " (nombre, edad) VALUE (\""+nombre+"\","+edad+");";
				
				Statement st = conexion.createStatement();
				st.execute(Query);
				
				System.out.println("	--> Datos introducidos correctamente\n");
			} catch (SQLException e) {
				System.out.println("No s'ha pogut introduir");
			}
	}
	
	
	//OBTENER ARTICUO
	public static void obtenerPelicula() {
		
		try {
			String Querydb = "USE "+ bd +";";
			Statement stdb = conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "SELECT * FROM " + tabla;
			Statement st = conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			
			while(resultSet.next()) {
				System.out.println("-------------\nCodigo:	" + resultSet.getInt("codigo") +"\nNombre: " + resultSet.getString("nombre")+"\nEdad: " + resultSet.getString("edad"));
			}			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error");
		}		
	}
	
	

	
	

	
	

}


