package act1ud18App;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.swing.JOptionPane;

import dto.Peliculas;
import dto.Salas;
import dto.Peliculas;


public class act1ud18App {

	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		final String bd = "act4";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.134:3306?"
					+ "useTimezone=true&serverTimezone=UTC","remote","12345Remote<3!");
			System.out.println("Bienvenido a los almacenes\n  ----------------------- ");
			
			eliminarDB(bd, conexion);
			createDB(bd, conexion); 
			
			System.out.println("----------- PELICULAS -----------");
			Peliculas peli = new Peliculas(conexion, bd, "Titanic", 12);
			Peliculas peli2 = new Peliculas(conexion, bd, "Simpsons", 7);
			Peliculas peli3 = new Peliculas(conexion, bd, "saw", 18);
			Peliculas peli4 = new Peliculas(conexion, bd, "La Selva del Camp", 100);
			Peliculas peli5 = new Peliculas(conexion, bd, "Valles", 130);
			
			
			System.out.println("--------------------------------------\n\nPELICULAS REGISTRADOS: ");
			peli.obtenerPelicula();
			
			System.out.println("\n\n------------ CAJAS ---------------");
			Salas sala = new Salas(conexion, bd, "sala1", 1);
			Salas sala2 = new Salas(conexion, bd, "sala2", 2);
			Salas sala3 = new Salas(conexion, bd, "sala3", 3);
			Salas sala4 = new Salas(conexion, bd, "sala4", 1);
			Salas sala5 = new Salas(conexion, bd, "sala5", 2);
			System.out.println("--------------------------------------\n\nCAJAS REGISTRADAS: ");
			sala.obtenerSalas();
			
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("No se ha podido conectar con la base de datos");
			System.out.println(e);
		}

	}
	
	

	// CREACI� DE LA BASE DE DADES
	public static void eliminarDB(String name, Connection conexion) {
		try {
			String Query = "DROP DATABASE " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (SQLException ex) {
		
		}
	}

	
	// CREACI� DE LA BASE DE DADES
	public static void createDB(String name, Connection conexion) {
		try {
			String Query = "CREATE DATABASE IF NOT EXISTS " + name;
			Statement st = conexion.createStatement();
			st.executeUpdate(Query);
		} catch (SQLException ex) {
			System.out.println("No se ha podido crear la base de datos");
		}
	}


	
	

}
