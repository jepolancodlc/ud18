package dto;

public class Almacenes {
	int codigoAlm, capacidad;
	String lugar;
	
	public Almacenes() {
		// TODO Auto-generated constructor stub
	}

	public Almacenes(int codigoAlm, String lugar, int capacidad) {
		this.codigoAlm = codigoAlm;
		this.capacidad = capacidad;
		this.lugar = lugar;
	}

	public int getCodigoAlm() {
		return codigoAlm;
	}

	public void setCodigoAlm(int codigoAlm) {
		this.codigoAlm = codigoAlm;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	
	
}
