package dto;

import java.sql.SQLException;
import java.sql.Statement;

public class DB {

	public static void crearDB(String name) {
		// TODO Auto-generated method stub
		try { String Query= "CREATE DATABASE IF NOT EXISTS " + name;
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("La base de datos " + name + " se ha creado");	
	
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("La base de datos no se ha podido crear");	
		}
	}
	
	/////ALMACENES
	public static void crearTablaAlm(String db, String name) {
		// TODO Auto-generated method stub
		try {
			String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(codigoAlm INT PRIMARY KEY, lugar NVARCHAR(100), capacidad INT);"; 
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void insertAlm(String db, String table, int codigoAlm, String lugar, int capacidad) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + table +"(codigoAlm, lugar, capacidad) VALUES(" 
					+ "\"" + codigoAlm + "\", "
					+ "\"" + lugar + "\", "
					+ "\"" + capacidad + "\"); ";			
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesAlm(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
		
			System.out.println("TABLA ALMACENES");

				while (resultSet.next()) {
					System.out.println("Cod. Alm: "+ resultSet.getString("codigoAlm")+ " | "
							+ "Lugar: " + resultSet.getString("lugar")+" | "
							+ "Capacidad: "+ resultSet.getString("capacidad"));
			}} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}
	/////Cajas
	public static void crearTablaC(String db, String name) {
		// TODO Auto-generated method stub
		try {
			String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "CREATE TABLE IF NOT EXISTS "+name+""
					+ "(numRef INT PRIMARY KEY, "
					+ "contenido NVARCHAR(100), "
					+ "valor INT, "
					+ "almacen INT,"
					+ "FOREIGN KEY (almacen) REFERENCES Almacenes(codigoAlm)); ";
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
			
		
	public static void insertC(String db, String table, int numRef, String contenido, int valor, int almacen) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + table +"(numRef, contenido, valor, almacen) VALUES(" 
					+ "\"" + numRef + "\", "
					+ "\"" + contenido + "\", "
					+ "\"" + valor+ "\", "
					+ "\"" + almacen+ "\"); ";		
		
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	public static void getValuesC(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
	
			System.out.println("TABLA EMPLEADOS");
			
				while (resultSet.next()) {
					System.out.println("Num. Ref: "+ resultSet.getString("numRef")+ " | "
						+ "Contenido: "+ resultSet.getString("contenido") +" | "
						+ "Valor: " + resultSet.getString("valor")+" | "
						+ "Num. Almacen: " + resultSet.getString("almacen"));
			}} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}
}

