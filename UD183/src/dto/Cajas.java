package dto;

public class Cajas {
	int numRef;
	String contenido;
	int valor, almacen;
	
	public Cajas() {
		// TODO Auto-generated constructor stub
	}

	public Cajas(int numRef, String contenido, int valor, int almacen) {
		this.numRef = numRef;
		this.contenido = contenido;
		this.valor = valor;
		this.almacen = almacen;
	}

	public int getNumRef() {
		return numRef;
	}

	public void setNumRef(char numRef) {
		this.numRef = numRef;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public int getAlmacen() {
		return almacen;
	}

	public void setAlmacen(int almacen) {
		this.almacen = almacen;
	}
}
