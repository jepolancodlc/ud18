package dto;

public class Ejecutadores {
	
	public static void exeAlm() {
		// TODO Auto-generated method stub
		
		 DB.crearTablaAlm("LosAlmacenes", "Almacenes");

		 Almacenes a1=new Almacenes(101, "Tarragona", 500);
		 DB.insertAlm("LosAlmacenes", "Almacenes",a1.getCodigoAlm(),a1.getLugar(), a1.getCapacidad());
		 
		 Almacenes a2=new Almacenes(102, "Lleida", 700);
		 DB.insertAlm("LosAlmacenes", "Almacenes",a2.getCodigoAlm(),a2.getLugar(), a2.getCapacidad());

		Almacenes a3=new Almacenes(103, "Barcelona", 100);
		 DB.insertAlm("LosAlmacenes", "Almacenes",a3.getCodigoAlm(),a3.getLugar(), a3.getCapacidad());

		 Almacenes a4=new Almacenes(104, "Valencia", 650);
		 DB.insertAlm("LosAlmacenes", "Almacenes",a4.getCodigoAlm(),a4.getLugar(), a4.getCapacidad());
		 
		 Almacenes a5=new Almacenes(105, "Lleida", 400);
		 DB.insertAlm("LosAlmacenes", "Almacenes",a5.getCodigoAlm(),a5.getLugar(), a5.getCapacidad());

		System.out.println("");}

	public static void exeCajas() {
		// TODO Auto-generated method stub
		 DB.crearTablaC("LosAlmacenes", "Cajas");
		 
		 Cajas c1=new Cajas(12345, "Pelotas", 103, 101);
		 DB.insertC("LosAlmacenes", "Cajas", c1.getNumRef(), c1.getContenido(), c1.getValor(), c1.getAlmacen());
		 
		 Cajas	c2=new Cajas(74123, "Guantes", 150, 103);
		 DB.insertC("LosAlmacenes", "Cajas", c2.getNumRef(), c2.getContenido(), c2.getValor(), c2.getAlmacen());
		 
		 Cajas	c3=new Cajas(36656, "Vasos", 80, 105);
		 DB.insertC("LosAlmacenes", "Cajas", c3.getNumRef(), c3.getContenido(), c3.getValor(), c3.getAlmacen());
		 
		 Cajas	c4=new Cajas(54831, "Discos", 20, 104);
		 DB.insertC("LosAlmacenes", "Cajas", c4.getNumRef(), c4.getContenido(), c4.getValor(), c4.getAlmacen());
		 
		 Cajas	c5=new Cajas(95175, "Gafas", 200, 102);
		 DB.insertC("LosAlmacenes", "Cajas", c5.getNumRef(), c5.getContenido(), c5.getValor(), c5.getAlmacen());

	System.out.println("");
	}	
}
