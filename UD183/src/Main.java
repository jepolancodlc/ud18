import dto.Conexiones;
import dto.DB;
import dto.Ejecutadores;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conexiones.conectar();
		DB.crearDB("LosAlmacenes");
		System.out.println("-------------------------");
		Ejecutadores.exeAlm();
		DB.getValuesAlm("LosAlmacenes", "Almacenes");
		System.out.println("-------------------------");
		Ejecutadores.exeCajas();
		DB.getValuesC("LosAlmacenes", "Cajas");
		System.out.println("-------------------------");

		Conexiones.desconectar();
	}
}

