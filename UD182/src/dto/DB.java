package dto;

import java.sql.SQLException;
import java.sql.Statement;

public class DB {

	public static void crearDB(String name) {
		// TODO Auto-generated method stub
		try { String Query= "CREATE DATABASE IF NOT EXISTS " + name;
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("La base de datos " + name + " se ha creado");	
	
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("La base de datos no se ha podido crear");	
		}
	}
	
	/////Departamentos
	public static void crearTablaDep(String db, String name) {
		// TODO Auto-generated method stub
		try {
			String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(codigoDep INT PRIMARY KEY, nombre NVARCHAR(100), presupuesto INT);"; 
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void insertDep(String db, String table, int codigoDep, String nombre, int presupuesto) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + table +"(codigoDep, nombre, presupuesto) VALUES(" 
					+ "\"" + codigoDep + "\", "
					+ "\"" + nombre + "\", "
					+ "\"" + presupuesto + "\"); ";			
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesDep(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
		
			System.out.println("TABLA DEPARTAMENTOS");

				while (resultSet.next()) {
					System.out.println("Cod. Dep: "+ resultSet.getString("codigoDep")+ " | "
							+ "Presupuesto: " + resultSet.getString("presupuesto")+" | "
							+ "Nombre: "+ resultSet.getString("nombre"));
			}} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}
	/////EMPLEADOS
	public static void crearTablaEmp(String db, String name) {
		// TODO Auto-generated method stub
		try {
			String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(dni VARCHAR(8) PRIMARY KEY, nombre NVARCHAR(100), apellido NVARCHAR(255), departamento INT);"; 
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
			
		
	public static void insertEmp(String db, String table, String dni, String nombre, String apellido, int departamento) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "INSERT INTO " + table +"(dni, nombre, apellido, departamento) VALUES(" 
					+ "\"" + dni + "\", "
					+ "\"" + nombre + "\", "
					+ "\"" + apellido + "\", "
					+ "\"" + departamento + "\"); ";		
		
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	public static void getValuesEmp(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
		
			System.out.println("TABLA EMPLEADOS");

				while (resultSet.next()) {
					System.out.println("DNI: "+ resultSet.getString("dni")+ " | "
						+ "Nombre: "+ resultSet.getString("nombre") +" | "
						+ "Apellido: " + resultSet.getString("apellido")+" | "
						+ "Departamento: " + resultSet.getString("departamento"));
			}} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}

}

