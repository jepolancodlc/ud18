package dto;

public class Departamentos {
	int codigoDep, presupuesto;
	String nombre;
	
	 public Departamentos() {
		// TODO Auto-generated constructor stub
	}

	public Departamentos(int codigoDep, String nombre, int presupuesto) {
		this.codigoDep = codigoDep;
		this.presupuesto = presupuesto;
		this.nombre = nombre;
	}

	public int getCodigoDep() {
		return codigoDep;
	}

	public void setCodigoDep(int codigoDep) {
		this.codigoDep = codigoDep;
	}

	public int getPresupuesto() {
		return presupuesto;
	}

	public void setPreuspuesto(int presupuesto) {
		this.presupuesto = presupuesto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	 
	 
	
}
