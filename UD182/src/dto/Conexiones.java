package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexiones {
	static Connection conexion;
	
	//Connecter y disconnecter
	public static Connection conectar() {
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conexion=DriverManager.getConnection("jdbc:mysql://192.168.1.76:3306?useTimezone=true&serverTimezone=UTC", "remote", "Jp@lanc07");
			System.out.println("Conexion establecida");
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			System.out.println("No se ha podido conectar al servidor");
			System.out.println(e);
		}
		return conexion;
	}
	
	public static void desconectar() {
		// TODO Auto-generated method stub
		try { conexion.close(); System.out.println("Se ha finalizado la conexion");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.print("Error al cerrar conexion");	
			System.out.println(e.getMessage());
		}
	}
}
