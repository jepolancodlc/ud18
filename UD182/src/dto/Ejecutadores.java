package dto;

public class Ejecutadores {
	
	public static void exeDep() {
		// TODO Auto-generated method stub
		
		 DB.crearTablaDep("E2", "Departamentos");

		 Departamentos d1=new Departamentos(101, "Financiero", 10000);
		 DB.insertDep("E2", "Departamentos",d1.codigoDep,d1.nombre, d1.presupuesto);
		 
		 Departamentos d2=new Departamentos(102, "RRHH", 12100);
		 DB.insertDep("E2", "Departamentos", d2.getCodigoDep(), d2.getNombre(), d2.getPresupuesto());
		 
		 Departamentos d3=new Departamentos(103, "Marketing", 11810);
		 DB.insertDep("E2", "Departamentos", d3.getCodigoDep(), d3.getNombre(), d3.getPresupuesto());
		 
		 Departamentos d4=new Departamentos(104, "IT", 25400);
		 DB.insertDep("E2", "Departamentos", d4.getCodigoDep(), d4.getNombre(), d4.getPresupuesto());
		 
		 Departamentos d5=new Departamentos(105, "Ciencias", 30000);
		 DB.insertDep("E2", "Departamentos", d5.getCodigoDep(), d5.getNombre(), d5.getPresupuesto());
			System.out.println("");}

	public static void exeEmp() {
		// TODO Auto-generated method stub
		 DB.crearTablaEmp("E2", "Empleados");
		
		Empleados e1=new Empleados("48287412", "Homer J.", "Simpson", 103);
		DB.insertEmp("E2", "Empleados", e1.getDni(), e1.getNombre(), e1.getApellido(), e1.getDepartamento());
		
		Empleados e2=new Empleados("82343242", "John", "Wick", 105);
		DB.insertEmp("E2", "Empleados", e2.getDni(), e2.getNombre(), e2.getApellido(), e2.getDepartamento());
		
		Empleados e3=new Empleados("76223432", "Maximo", "Decimo Meridio", 102);
		DB.insertEmp("E2", "Empleados", e3.getDni(), e3.getNombre(), e3.getApellido(), e3.getDepartamento());
		
		Empleados e4=new Empleados("21244919", "Ash", "Ketchum", 101);
		DB.insertEmp("E2", "Empleados", e4.getDni(), e4.getNombre(), e4.getApellido(), e4.getDepartamento());
		
		Empleados e5=new Empleados("36543210", "Steve", "Jobs", 104);
		DB.insertEmp("E2", "Empleados", e5.getDni(), e5.getNombre(), e5.getApellido(), e5.getDepartamento());
		
		System.out.println("");
	}	
}
