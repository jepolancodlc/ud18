import dto.Conexiones;
import dto.DB;
import dto.Ejecutadores;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conexiones.conectar();
		DB.crearDB("E2");
		System.out.println("--------------------------------------");

		Ejecutadores.exeDep();
		DB.getValuesDep("E2", "Departamentos"); 
		System.out.println("--------------------------------------");
		Ejecutadores.exeEmp();
		DB.getValuesEmp("E2", "Empleados");
	
		Conexiones.desconectar();

	}

}
