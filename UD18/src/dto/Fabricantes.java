package dto;

public class Fabricantes {

	static int codigoFab;	static String nombre;
	
	public Fabricantes(int codigo, String nombre) {
		Fabricantes.codigoFab = codigo;
		Fabricantes.nombre = nombre;
	}
	
	public Fabricantes() {
	}

	//Get&Set
	public int getCodigoFab() {
		return codigoFab;
	}
	public void setCodigoFab(int codigo) {
		Fabricantes.codigoFab = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		Fabricantes.nombre = nombre;
	}

	
}



