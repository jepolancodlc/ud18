package dto;

public class Ejecutadores {

	public static void exeF() {
		// TODO Auto-generated method stub
		
		 DB.crearTablaF("TiendaInformatica", "Fabricantes");

		 Fabricantes fab1 =new Fabricantes(100,"LG");		 
		 DB.insertF("TiendaInformatica", "Fabricantes", fab1.getCodigoFab(), fab1.getNombre());
		
		 Fabricantes fab2 =new Fabricantes(200,"Apple");
		 DB.insertF("TiendaInformatica", "Fabricantes", fab2.getCodigoFab(), fab2.getNombre());
		 Fabricantes fab3 =new Fabricantes(300,"LG");

		 DB.insertF("TiendaInformatica", "Fabricantes", fab3.getCodigoFab(), fab3.getNombre());
		 Fabricantes fab4 =new Fabricantes(400,"AlienWare");

		 DB.insertF("TiendaInformatica", "Fabricantes", fab4.getCodigoFab(), fab4.getNombre());
		 
		 Fabricantes fab5 =new Fabricantes(500,"HP");
		 DB.insertF("TiendaInformatica", "Fabricantes", fab5.getCodigoFab(), fab5.getNombre());
			System.out.println("");
}
	
	public static void exeA() {
		// TODO Auto-generated method stub
		DB.crearTablaA("TiendaInformatica", "Articulos");

		//Articulos art5 =new Articulos(codigoA, nombre, fabricante, precio)
		Articulos art1 =new Articulos(1,"Movil", 500, 100);
		DB.insertA("TiendaInformatica", "Articulos", art1.getCodigoA(), art1.getNombre(), art1.getPrecio(), art1.getFabricante());
				 
		Articulos art2 =new Articulos(4,"Watch", 400, 280);
		DB.insertA("TiendaInformatica", "Articulos", art2.getCodigoA(), art2.getNombre(), art2.getPrecio(), art2.getFabricante());

		Articulos art3 =new Articulos(2,"TV", 300, 1000);
		DB.insertA("TiendaInformatica", "Articulos", art3.getCodigoA(), art3.getNombre(), art3.getPrecio(), art3.getFabricante());

		Articulos art4 =new Articulos(3,"PC", 200, 1700);
		DB.insertA("TiendaInformatica", "Articulos", art4.getCodigoA(), art4.getNombre(), art4.getPrecio(), art4.getFabricante());
 
		Articulos art5 =new Articulos(5,"Tablet", 100, 200);
		DB.insertA("TiendaInformatica", "Articulos", art5.getCodigoA(), art5.getNombre(), art5.getPrecio(), art5.getFabricante());
		System.out.println("");
}
}
