package dto;

public class Articulos {
	
	int codigoA, fabricante, precio; String nombre;
	
	//Cons
	public Articulos() {
		// TODO Auto-generated constructor stub
	}
	public Articulos(int codigoA, String nombre, int fabricante, int precio) {
		// TODO Auto-generated constructor stub
		this.codigoA = codigoA;
		this.nombre = nombre;
		this.fabricante = fabricante;
		this.precio = precio;

		
	//Get&Set
	}
	public int getCodigoA() {
		return codigoA;
	}
	public void setCodigoA(int codigo) {
		this.codigoA = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getFabricante() {
		return fabricante;
	}
	public void setFabricante(int fabricante) {
		this.fabricante = fabricante;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	
}
