package dto;
import java.sql.SQLException;
import java.sql.Statement;

public class DB {
	
	public static void crearDB(String name) {
		// TODO Auto-generated method stub
		try { String Query= "CREATE DATABASE IF NOT EXISTS " + name;
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("La base de datos " + name + " se ha creado");	
	
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("La base de datos no se ha podido crear");	
		}
	}
	
	////////////////FABRICANTES
	public static void crearTablaF(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(codigoFab INT PRIMARY KEY, nombre NVARCHAR(100));";
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}

		
	public static void insertF(String db, String table_name, int codigofab, String name) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + table_name + " VALUES(" 
			+ codigofab + ", "+"'"+ name +"' );";
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesF(String db, String table_name) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
							
			String Query = "SELECT * FROM " + table_name;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			System.out.println("TABLA FABRICANTES");
			
				while (resultSet.next()) {
					System.out.println(	"Cod. Fabricantes: " +  resultSet.getString("codigofab")  +" || "
					+ "Nombre: " 	+  resultSet.getString("nombre"));
	
				}} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}	
	

	////////////////ARTICULOS 
	public static void crearTablaA(String db,String name) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb = Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
				
			String Query = "CREATE TABLE IF NOT EXISTS "+name+""
					+ "(codigo INT PRIMARY KEY, "
					+ "nombre NVARCHAR(100), "
					+ "precio INT, "
					+ "fabricante INT, "
					+ "FOREIGN KEY (fabricante) REFERENCES Fabricantes(codigofab));";
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" creada");
		}catch (SQLException e){
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");	
		}
			
	}
	
	public static void insertA(String db, String table_name, int codigo, String nombre,int precio, int fabricante) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
							
			String Query = "INSERT INTO " + table_name + " (codigo, nombre, precio, fabricante)VALUES(" 
			+ codigo + ", "+"'"+ nombre +"'"+ "," + precio + "," + fabricante  +");";
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");
				
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesA(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
		Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
		
			System.out.println("TABLA ARTICULOS");

				while (resultSet.next()) {
					System.out.println("Cod. Articulo: "+ resultSet.getString("codigo")+ " | "
						+ "Nombre: "+ resultSet.getString("nombre") +" | "
						+ "Precio: " + resultSet.getString("Precio")+" | "
						+ "Fabricante: " + resultSet.getString("fabricante"));
			}} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}

}