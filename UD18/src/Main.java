import dto.Conexiones;
import dto.DB;
import dto.Ejecutadores;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
	Conexiones.conectar();
	
	DB.crearDB("TiendaInformatica");
	System.out.println("-------------------------");
	Ejecutadores.exeF();
	DB.getValuesF("TiendaInformatica", "Fabricantes");
	System.out.println("-------------------------");
	Ejecutadores.exeA();
	DB.getValuesA("TiendaInformatica", "Articulos");
	System.out.println("-------------------------");

	Conexiones.desconectar();
	 }

	
}
