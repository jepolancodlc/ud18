package obj;

import java.sql.SQLException;
import java.sql.Statement;

import def.Conexiones;

public class Ventas {
	int cajero, maquina, producto;
	
	public Ventas() {
		// TODO Auto-generated constructor stub
	}

	public Ventas(int cajero, int maquina, int producto) {
		this.cajero = cajero;
		this.maquina = maquina;
		this.producto = producto;
	}

	public int getCajero() {
		return cajero;
	}

	public void setCajero(int cajero) {
		this.cajero = cajero;
	}

	public int getMaquina() {
		return maquina;
	}

	public void setMaquina(int maquina) {
		this.maquina = maquina;
	}

	public int getProducto() {
		return producto;
	}

	public void setProducto(int producto) {
		this.producto = producto;
	}
	
	public static void tablaVentas(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String Query = "CREATE TABLE IF NOT EXISTS "+name+""
					+ "(cajero INT PRIMARY KEY, "
					+ "maquina INT, "
					+ "producto INT,"
					+ "FOREIGN KEY (cajero) REFERENCES Cajeros(codigoCaj),"
					+ "FOREIGN KEY (maquina) REFERENCES Maquinas(codigoMaq),"
					+ "FOREIGN KEY (producto) REFERENCES Productos(codigoPro)); ";
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void insertVentas(String db, String tabla, int cajero, int maquina, int producto) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tabla +"(cajero, maquina, producto) VALUES(" 
					+ "\"" + cajero + "\", "
					+ "\"" + maquina + "\", "
					+ "\"" + producto+ "\"); ";		
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesCaja(String db, String tabla) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
							
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			System.out.println("TABLA VENTAS\n---------");
			
				while (resultSet.next()) {
					System.out.println(	"Cod. Cajero: " +  resultSet.getString("cajero")  +" || "
					+ "Maquina Registradora: " 	+  resultSet.getString("maquina")  +" || "
					+"Producto: " +  resultSet.getString("producto"));
	
				}} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}	
}


