package obj;

import java.sql.SQLException;
import java.sql.Statement;

import def.Conexiones;

public class MaqR {
	int codigoMaq,piso;
	public MaqR() {
		// TODO Auto-generated constructor stub
	}
	public MaqR(int codigoMaq, int piso) {
		this.codigoMaq = codigoMaq;
		this.piso = piso;
	}
	public int getCodigoMaq() {
		return codigoMaq;
	}
	public void setCodigoMaq(int codigoMaq) {
		this.codigoMaq = codigoMaq;
	}
	public int getPiso() {
		return piso;
	}
	public void setPiso(int piso) {
		this.piso = piso;
	}
	
	public static void tablaMaq(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(codigoMaq int primary key,"
					+ "piso int);";
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void insertMaq(String db, String tabla, int codigoMaq, int piso) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tabla + " (codigoMaq, piso)VALUES(" 
					+codigoMaq+", " +"'"+ piso +"'"  +");";

			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesMaq(String db, String tabla) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
							
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			System.out.println("TABLA MAQUINAS\n---------");
			
				while (resultSet.next()) {
					System.out.println(	"Cod. Cajero: " +  resultSet.getString("codigoMaq")  +" || "
					+ "Piso #: " 	+  resultSet.getString("piso"));
	
				}} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}	
}
