package obj;

import java.sql.SQLException;
import java.sql.Statement;

import def.Conexiones;

public class Productos {
	int codigoPro, precio;
	String nombre;
	
	public Productos() {
		// TODO Auto-generated constructor stub
	}

	public Productos(int codigoPro, String nombre, int precio) {
		this.codigoPro = codigoPro;
		this.precio = precio;
		this.nombre = nombre;
	}

	public int getCodigoPro() {
		return codigoPro;
	}

	public void setCodigoPro(int codigoPro) {
		this.codigoPro = codigoPro;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public static void tablaPro(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(codigoPro int primary key,"
					+ "nombre nvarchar(255)"
					+ ",precio int);";
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void insertPro(String db, String tabla, int codigoPro, String nombre, int precio) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tabla +"(codigoPro, nombre, precio) VALUES(" 
					+ "\"" + codigoPro + "\", "
					+ "\"" + nombre + "\", "
					+ "\"" + precio + "\"); ";		

			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesCaja(String db, String tabla) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
							
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			System.out.println("TABLA PRODUCTOS\n---------");
			
				while (resultSet.next()) {
					System.out.println(	
					"Cod. Cajero: " +  resultSet.getString("codigoPro")  +" | "
					+ "Nombre: " 	+  resultSet.getString("nombre") +" | "
					+ "Precio: " + resultSet.getString("precio"));
	
				}} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}		
}
