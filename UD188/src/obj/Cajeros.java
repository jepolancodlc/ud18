package obj;

import java.sql.SQLException;
import java.sql.Statement;

import def.Conexiones;
	
public class Cajeros {
	
	
	int codigoCaj;
	String nombre;
 
	public Cajeros() {
		// TODO Auto-generated constructor stub
	}

	public Cajeros(int codigoCaj, String nombre) {
		this.codigoCaj = codigoCaj;
		this.nombre = nombre;
	}

	public int getCodigoCaj() {
		return codigoCaj;
	}

	public void setCodigoCaj(int codigoCaj) {
		this.codigoCaj = codigoCaj;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public static void tablaCaja(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(codigoCaj int primary key,"
					+ "nombre nvarchar(255));";
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void insertCaja(String db, String tabla, int codigoCaj, String nombre) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + tabla + " (codigoCaj, nombre)VALUES(" 
					+codigoCaj+", " +"'"+ nombre +"'"  +");";
			Statement st = Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesCaja(String db, String tabla) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexiones.conexion.createStatement();
			stdb.executeUpdate(Querydb);
							
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexiones.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			System.out.println("TABLA CAJEROS\n---------");
			
				while (resultSet.next()) {
					System.out.println(	"Cod. Cajero: " +  resultSet.getString("codigoCaj")  +" || "
					+ "Nombre: " 	+  resultSet.getString("nombre"));
	
				}} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}	
}
