package def;

import java.sql.SQLException;
import java.sql.Statement;


public class DB {
	public static void crearDB(String name) {
		// TODO Auto-generated method stub
		try { String Query= "CREATE DATABASE IF NOT EXISTS " + name;
			Statement st= Conexiones.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("La base de datos " + name + " se ha creado");	
	
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("La base de datos no se ha podido crear");	
		}
	}
}
