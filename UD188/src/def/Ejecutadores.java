package def;

import obj.Cajeros;
import obj.MaqR;
import obj.Productos;
import obj.Ventas;

public class Ejecutadores {
	
	 public static void exeCajero() {
		// TODO Auto-generated method stub
		 Cajeros.tablaCaja("GrandesAlmacenes", "Cajeros"); 
		 
		 Cajeros c1=new Cajeros(11, "Juan");
		 Cajeros.insertCaja("GrandesAlmacenes", "Cajeros", c1.getCodigoCaj(), c1.getNombre());

		 Cajeros c2=new Cajeros(22, "Merce");
		 Cajeros.insertCaja("GrandesAlmacenes", "Cajeros", c2.getCodigoCaj(), c2.getNombre());

		 Cajeros c3=new Cajeros(33, "Pedro");
		 Cajeros.insertCaja("GrandesAlmacenes", "Cajeros", c3.getCodigoCaj(), c3.getNombre());
		 
		 Cajeros c4=new Cajeros(44, "Leon");
		 Cajeros.insertCaja("GrandesAlmacenes", "Cajeros", c4.getCodigoCaj(), c4.getNombre());
		 
		 Cajeros c5=new Cajeros(55, "Anna");
		 Cajeros.insertCaja("GrandesAlmacenes", "Cajeros", c5.getCodigoCaj(), c5.getNombre());
		 
	}
	 
	 public static void exeMaquina() {
		MaqR.tablaMaq("GrandesAlmacenes", "Maquinas");
		
		MaqR  m1 = new MaqR(801, 1);
		MaqR.insertMaq("GrandesAlmacenes", "Maquinas", m1.getCodigoMaq(), m1.getPiso());

		MaqR  m2 = new MaqR(802, 2);
		MaqR.insertMaq("GrandesAlmacenes", "Maquinas", m2.getCodigoMaq(), m2.getPiso());

		MaqR  m3 = new MaqR(803, 3);
		MaqR.insertMaq("GrandesAlmacenes", "Maquinas", m3.getCodigoMaq(), m3.getPiso());

		MaqR  m4 = new MaqR(804, 4);
		MaqR.insertMaq("GrandesAlmacenes", "Maquinas", m4.getCodigoMaq(), m4.getPiso());

		MaqR  m5 = new MaqR(805, 5);
		MaqR.insertMaq("GrandesAlmacenes", "Maquinas", m5.getCodigoMaq(), m5.getPiso()); 
		
	}
	 
	 public static void exePro() {
		Productos.tablaPro("GrandesAlmacenes", "Productos");
		
		Productos p1=new Productos(101, "Mesa", 100);
		Productos.insertPro("GrandesAlmacenes", "Productos", p1.getCodigoPro(), p1.getNombre(), p1.getPrecio());
		
		Productos p2=new Productos(102, "Puerta", 110);
		Productos.insertPro("GrandesAlmacenes", "Productos", p2.getCodigoPro(), p2.getNombre(), p2.getPrecio());
		
		Productos p3=new Productos(103, "TV", 400);
		Productos.insertPro("GrandesAlmacenes", "Productos", p3.getCodigoPro(), p3.getNombre(), p3.getPrecio());
		
		Productos p4=new Productos(104, "Cuadro", 200);
		Productos.insertPro("GrandesAlmacenes", "Productos", p4.getCodigoPro(), p4.getNombre(), p4.getPrecio());
		
		Productos p5=new Productos(105, "Armario", 600);
		Productos.insertPro("GrandesAlmacenes", "Productos", p5.getCodigoPro(), p5.getNombre(), p5.getPrecio()); 
	}
	 
	public static void exeVentas() {
		
	Ventas.tablaVentas("GrandesAlmacenes", "Ventas");

	Ventas v1=new Ventas(11, 801, 101);
	Ventas.insertVentas("GrandesAlmacenes", "Ventas", v1.getCajero(), v1.getMaquina(), v1.getProducto());
	
	Ventas v2=new Ventas(22, 802, 102);
	Ventas.insertVentas("GrandesAlmacenes", "Ventas", v2.getCajero(), v2.getMaquina(), v2.getProducto());
	
	Ventas v3=new Ventas(33, 803, 103);
	Ventas.insertVentas("GrandesAlmacenes", "Ventas", v3.getCajero(), v3.getMaquina(), v3.getProducto());
	
	Ventas v4=new Ventas(44, 804, 104);
	Ventas.insertVentas("GrandesAlmacenes", "Ventas", v4.getCajero(), v4.getMaquina(), v4.getProducto());
	
	Ventas v5=new Ventas(55, 805, 105);
	Ventas.insertVentas("GrandesAlmacenes", "Ventas", v5.getCajero(), v5.getMaquina(), v5.getProducto());

	}
}
