import def.Conexiones;
import def.DB;
import def.Ejecutadores;
import obj.Cajeros;
import obj.MaqR;
import obj.Productos;
import obj.Ventas;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conexiones.conectar();
		System.out.println("**********************");
		DB.crearDB("GrandesAlmacenes");
		System.out.println("**********************");
		
		Ejecutadores.exeCajero();
		System.out.println("");

		Cajeros.getValuesCaja("GrandesAlmacenes", "Cajeros");
		System.out.println("**********************");

		Ejecutadores.exeMaquina();
		System.out.println("");

		MaqR.getValuesMaq("GrandesAlmacenes", "Maquinas");
		System.out.println("**********************");

		Ejecutadores .exePro();
		System.out.println("");

		Productos.getValuesCaja("GrandesAlmacenes", "Productos");
		System.out.println("**********************");

		Ejecutadores.exeVentas();
		System.out.println("");
		Ventas.getValuesCaja("GrandesAlmacenes", "Ventas");

		Conexiones.desconectar();

	}

}
