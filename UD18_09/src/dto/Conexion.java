package dto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Conexion {
	
	public static Connection conexion;
	
	//Conexi�n con el servidor Mysql
		public static void conectar() {
			
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conexion = DriverManager.getConnection("jdbc:mysql://192.168.0.14:3306?useTimezone=true&serverTimezone=UTC","------","------"); //credenciales temporales
				System.out.print("Servidor Conectado");
				fecha();
				
			}catch(SQLException | ClassNotFoundException ex  ){
				System.out.print("No se ha podido conectar con mi base de datos");
				fecha();
				System.out.println(ex.getMessage());
			}
			
		}
			
		//Desconecta la conexi�n con el servidor
		public void desconectar() {
			try {
		
				conexion.close();
				System.out.print("Servidor desconectado");
				fecha();
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
				System.out.print("Error cerrando conexion");
				fecha();
			}
		}
		
		//Mostra la fecha
		public static void fecha() {
			Date fecha = new Date();
			DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
			System.out.println(" - " + hourdateFormat.format(fecha));
		}
}
