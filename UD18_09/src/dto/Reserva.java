/**
 * 
 */
package dto;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Reserva {

	private String dni;
	private String numSerie;
	private String comienzo;
	private String fin;
	
	/**
	 * @param dni
	 * @param numSerie
	 * @param comienzo
	 * @param fin
	 */
	public Reserva(String dni, String numSerie, String comienzo, String fin) {
		this.dni = dni;
		this.numSerie = numSerie;
		this.comienzo = comienzo;
		this.fin = fin;
	}
	
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the numSerie
	 */
	public String getNumSerie() {
		return numSerie;
	}
	/**
	 * @param numSerie the numSerie to set
	 */
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
	/**
	 * @return the comienzo
	 */
	public String getComienzo() {
		return comienzo;
	}
	/**
	 * @param comienzo the comienzo to set
	 */
	public void setComienzo(String comienzo) {
		this.comienzo = comienzo;
	}
	/**
	 * @return the fin
	 */
	public String getFin() {
		return fin;
	}
	/**
	 * @param fin the fin to set
	 */
	public void setFin(String fin) {
		this.fin = fin;
	}
	
	
}
