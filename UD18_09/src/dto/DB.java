/**
 * 
 */
package dto;

import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class DB {
	public static void crearBD(String nombre) {
		try {
			String Query="CREATE DATABASE IF NOT EXISTS "+ nombre;
			Statement st= Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Base de datos creada con �xito!");
			
		JOptionPane.showMessageDialog(null, "Se ha creado la Base de Datos " +nombre+ " correctamente");
		
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error al crear la Base de Datos.");
		}	
	}
	
	public static void crearTablaF(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(codigo INT PRIMARY KEY, nombre NVARCHAR(100));";
			Statement st= Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}

	public static void crearTablaI(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(dni VARCHAR(8) PRIMARY KEY, nomaApels NVARCHAR(255), facultad INT, FOREIGN KEY (facultad) REFERENCES facultad(codigo));";
			Statement st= Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void crearTablaR(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(dni VARCHAR(8), numSerie CHAR(4), comienzo DATETIME, fin DATETIME, FOREIGN KEY (dni) REFERENCES investigadores(dni), FOREIGN KEY (numSerie) REFERENCES equipos(numSerie));";
			Statement st= Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
	
	public static void crearTablaE(String db, String name) {
		// TODO Auto-generated method stub
		try {String Querydb="Use "+db+ ";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
	
			String Query = "CREATE TABLE IF NOT EXISTS " + name + ""
					+ "(numSerie CHAR(4) PRIMARY KEY, nombre NVARCHAR(100), facultad int, FOREIGN KEY (facultad) REFERENCES facultad(codigo));";
			Statement st= Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Tabla "+name+" Creada");
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error al crear la tabla.");
		}
	}
		
	public static void insertF(String db, String table_name, int codigo, String nombre) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + table_name + " VALUES("+ codigo + ",'"+ nombre +"' );";
			Statement st = Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void insertI(String db, String table_name, String dni, String nomApels, int facultad) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + table_name + " VALUES('"+ dni + "','"+ nomApels +"',"+ facultad +");";
			Statement st = Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void insertR(String db, String table_name, String dni, String numSerie, String comienzo, String fin) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + table_name + " VALUES('"+ dni + "', '"+ numSerie +"',date('"+ comienzo +"'),date('" +fin+ "'));";
			Statement st = Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void insertE(String db, String table_name, String numSerie, String nombre, int facultad) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
			
			String Query = "INSERT INTO " + table_name + " VALUES('"+ numSerie + "', '"+ nombre +"','"+ facultad +"');";
			Statement st = Conexion.conexion.createStatement();
			st.executeUpdate(Query);
			System.out.println("Datos almacenados correctamente");	
			
		} catch (SQLException e ) {
				System.out.println(e.getMessage());
				System.out.println("Error en el almacenamiento"); 
		}
	}
	
	public static void getValuesF(String db, String table_name) {
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
							
			String Query = "SELECT * FROM " + table_name;
			Statement st = Conexion.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
			System.out.println("TABLA FACULTAD");
			
				while (resultSet.next()) {
					System.out.println(	"Cod. Facultad: " +  resultSet.getString("codigo")  +" || "
					+ "Nombre: " 	+  resultSet.getString("nombre"));
	
				}} catch (SQLException e) {
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}
	
	public static void getValuesI(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexion.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
		
			System.out.println("TABLA ARTICULOS");

			while (resultSet.next()) {
				System.out.println("DNI. Investigadores: "+ resultSet.getString("dni")+ " | "
					+ "Nombre y apellidos: "+ resultSet.getString("nombreApels") +" | "
					+ "Facultad: " + resultSet.getString("facultad"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}

	public static void getValuesR(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexion.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
		
			System.out.println("TABLA ARTICULOS");

			while (resultSet.next()) {
				System.out.println("DNI. Investigadores: "+ resultSet.getString("dni")+ " | "
					+ "Numero de Serie. Equipos: "+ resultSet.getString("numSerie") +" | "
					+ "Comienzo: " + resultSet.getString("comienzo")
					+ "fin: " + resultSet.getString("fin"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}
		
	public static void getValuesE(String db, String tabla) {
		// TODO Auto-generated method stub
		try {
			String Querydb = "USE "+db+";";
			Statement stdb= Conexion.conexion.createStatement();
			stdb.executeUpdate(Querydb);
								
			String Query = "SELECT * FROM " + tabla;
			Statement st = Conexion.conexion.createStatement();
			java.sql.ResultSet resultSet;
			resultSet = st.executeQuery(Query);
		
			System.out.println("TABLA ARTICULOS");

			while (resultSet.next()) {
				System.out.println("Numero de Serie. Equipos: "+ resultSet.getString("numSerie") +" | "
					+ "Nombre: " + resultSet.getString("nombre")
					+ "facultad: " + resultSet.getString("facultad"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			System.out.println("Error en la adquisicion de datos");
		}
	}
	
}
