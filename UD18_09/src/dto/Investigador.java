/**
 * 
 */
package dto;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Investigador {

	private String dni;
	private String nomApels;
	private int facultad;
	
	
	/**
	 * @param dni
	 * @param nomApels
	 * @param facultad
	 */
	public Investigador(String dni, String nomApels, int facultad) {
		this.dni = dni;
		this.nomApels = nomApels;
		this.facultad = facultad;
	}
	
	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	/**
	 * @return the nomApels
	 */
	public String getNomApels() {
		return nomApels;
	}
	/**
	 * @param nomApels the nomApels to set
	 */
	public void setNomApels(String nomApels) {
		this.nomApels = nomApels;
	}
	/**
	 * @return the facultad
	 */
	public int getFacultad() {
		return facultad;
	}
	/**
	 * @param facultad the facultad to set
	 */
	public void setFacultad(int facultad) {
		this.facultad = facultad;
	}
	
	
}
