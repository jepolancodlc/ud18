/**
 * 
 */
package dto;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Equipo {
	
	private String numSerie;
	private String nombre;
	private int facultad;
	
	/**
	 * @param numSerie
	 * @param nombre
	 * @param facultad
	 */
	public Equipo(String numSerie, String nombre, int facultad) {
		super();
		this.numSerie = numSerie;
		this.nombre = nombre;
		this.facultad = facultad;
	}

	/**
	 * @return the numSerie
	 */
	public String getNumSerie() {
		return numSerie;
	}

	/**
	 * @param numSerie the numSerie to set
	 */
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the facultad
	 */
	public int getFacultad() {
		return facultad;
	}

	/**
	 * @param facultad the facultad to set
	 */
	public void setFacultad(int facultad) {
		this.facultad = facultad;
	}
	
	
}
