/**
 * 
 */
package dto;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Ejecutador {
	
	public static void exeF() {
		// TODO Auto-generated method stub
		
		 DB.crearTablaF("Investigadores", "facultad");

		 Facultad f1 = new Facultad(1,"Universidade de Barcelona");		 
		 DB.insertF("Investigadores", "facultad", f1.getCodigo(), f1.getNombre());
		 Facultad f2 = new Facultad(2,"Universidade Carlos III");		 
		 DB.insertF("Investigadores", "facultad", f2.getCodigo(), f2.getNombre());
		 Facultad f3 = new Facultad(3,"Universidade Polit�cnica de Madrid");		 
		 DB.insertF("Investigadores", "facultad", f3.getCodigo(), f3.getNombre());
		 Facultad f4 = new Facultad(4,"Universidade Pompeu Fabra");		 
		 DB.insertF("Investigadores", "facultad", f4.getCodigo(), f4.getNombre());
		 Facultad f5 = new Facultad(5,"Universidade de Val�ncia");		 
		 DB.insertF("Investigadores", "facultad", f5.getCodigo(), f5.getNombre());
		 
		 DB.crearTablaI("Investigadores", "investigadores");
		 
		 Investigador i1 = new Investigador("A6781231", "Antonio Berenguer", 1);
		 DB.insertI("Investigadores", "investigadores", i1.getDni(), i1.getNomApels(), i1.getFacultad());
		 Investigador i2 = new Investigador("C6214512", "Victor Molina", 2);
		 DB.insertI("Investigadores", "investigadores", i2.getDni(), i2.getNomApels(), i2.getFacultad());
		 Investigador i3 = new Investigador("R2325421", "Maria Fernandes", 3);
		 DB.insertI("Investigadores", "investigadores", i3.getDni(), i3.getNomApels(), i3.getFacultad());
		 Investigador i4 = new Investigador("X1445123", "Nuria Sena", 4);
		 DB.insertI("Investigadores", "investigadores", i4.getDni(), i4.getNomApels(), i4.getFacultad());
		 Investigador i5 = new Investigador("Y1235465", "Marcon Vilalta", 5);
		 DB.insertI("Investigadores", "investigadores", i5.getDni(), i5.getNomApels(), i5.getFacultad());
	
		 DB.crearTablaE("Investigadores", "equipos");
		 
		 Equipo e1 = new Equipo("#GHY", "Equipo 1", 1);
		 DB.insertE("Investigadores", "equipos", e1.getNumSerie(), e1.getNombre(), e1.getFacultad());
		 Equipo e2 = new Equipo("#FDJ", "Equipo 2", 2);
		 DB.insertE("Investigadores", "equipos", e2.getNumSerie(), e2.getNombre(), e2.getFacultad());
		 Equipo e3 = new Equipo("#GEW", "Equipo 3", 3);
		 DB.insertE("Investigadores", "equipos", e3.getNumSerie(), e3.getNombre(), e3.getFacultad());
		 Equipo e4 = new Equipo("#L�L", "Equipo 4", 4);
		 DB.insertE("Investigadores", "equipos", e4.getNumSerie(), e4.getNombre(), e4.getFacultad());
		 Equipo e5 = new Equipo("#JJK", "Equipo 5", 5);
		 DB.insertE("Investigadores", "equipos", e5.getNumSerie(), e5.getNombre(), e5.getFacultad());
		 
		 DB.crearTablaR("Investigadores", "reserva");
		 
		 Reserva r1 = new Reserva("A6781231", "#GHY", "2020-07-01", "2020-08-31");
		 DB.insertR("Investigadores", "reserva", r1.getDni(), r1.getNumSerie(), r1.getComienzo(), r1.getFin());
		 Reserva r2 = new Reserva("C6214512", "#FDJ", "2020-03-15", "2020-04-15");
		 DB.insertR("Investigadores", "reserva", r2.getDni(), r2.getNumSerie(), r2.getComienzo(), r2.getFin());
		 Reserva r3 = new Reserva("R2325421", "#GEW", "2020-09-01", "2020-09-28");
		 DB.insertR("Investigadores", "reserva", r3.getDni(), r3.getNumSerie(), r3.getComienzo(), r3.getFin());
		 Reserva r4 = new Reserva("X1445123", "#L�L", "2020-12-01", "2020-12-31");
		 DB.insertR("Investigadores", "reserva", r4.getDni(), r4.getNumSerie(), r4.getComienzo(), r4.getFin());
		 Reserva r5 = new Reserva("Y1235465", "#JJK", "2020-10-05", "2020-11-25");
		 DB.insertR("Investigadores", "reserva", r5.getDni(), r5.getNumSerie(), r5.getComienzo(), r5.getFin());
	}

}
