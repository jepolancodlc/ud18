import dto.Conexion;
import dto.DB;
import dto.Ejecutador;

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class InvestigadoresApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Conexion.conectar();
		
		DB.crearBD("Investigadores");
		System.out.println("-------------------------");
		Ejecutador.exeF();

	}

}
